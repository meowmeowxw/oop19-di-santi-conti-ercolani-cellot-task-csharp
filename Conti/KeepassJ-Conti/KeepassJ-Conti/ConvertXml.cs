﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace KeepassJ_Conti
{
    static class ConvertXml
    {
        public static string ToXml(Database db)
        {
            string pad = "    ";
            string temp = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>" + System.Environment.NewLine;
            temp += "<Database>" + System.Environment.NewLine;
            temp += pad + "<NomeDatabase>" + db.Nome + "</NomeDatabase>" + System.Environment.NewLine;
            temp += pad + "<EntryList>" + System.Environment.NewLine;
            foreach (Entry item in db.ListEntry)
            {
                temp += pad + pad + "<Entry>" + System.Environment.NewLine;
                temp += pad + pad + pad + "<nomeEntry>" + item.NameEntry + "</nomeEntry>" + System.Environment.NewLine;
                temp += pad + pad + pad + "<Username>" + item.Username + "</Username>" + System.Environment.NewLine;
                temp += pad + pad + pad + "<Password>" + item.Password + "</Password>" + System.Environment.NewLine;
                temp += pad + pad + pad + "<Group>" + item.GroupName + "</Group>" + System.Environment.NewLine;
                temp += pad + pad + "</Entry>" + System.Environment.NewLine;
            }
            temp += pad + "</EntryList>" + System.Environment.NewLine;
            temp += pad + "<GroupList>" + System.Environment.NewLine;
            foreach (Group item in db.ListGroup)
            {
                temp += pad + pad + "<Group>" + System.Environment.NewLine;
                temp += pad + pad + pad + "<Name>" + item.Name + "</Name>" + System.Environment.NewLine;
                temp += pad + pad + pad + "<Description>" + item.Description + "</Description>" + System.Environment.NewLine;
                temp += pad + pad + "</Group>" + System.Environment.NewLine;
            }
            temp += pad + "</GroupList>" + System.Environment.NewLine;
            temp += "</Database>";
            return temp;
        }
    }
}
