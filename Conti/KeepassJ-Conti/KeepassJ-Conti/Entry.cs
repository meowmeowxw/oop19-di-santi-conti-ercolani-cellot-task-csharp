﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepassJ_Conti
{
    class Entry
    {
        public string NameEntry { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string GroupName { get; set; }

        public Entry(string nameEntry, string username, string password, Group group)
        {
            this.NameEntry = nameEntry;
            this.Username = username;
            this.Password = password;
            this.GroupName = group.Name;
        }
    }
}
