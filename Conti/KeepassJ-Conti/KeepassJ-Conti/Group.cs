﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeepassJ_Conti
{
    class Group
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public Group()
        {
            this.Name = "prova";
            this.Description = "prova";
        }

        public Group(string name)
        {
            this.Name = name;
            this.Description = "";
        }

        public Group(string name, string description)
        {
            this.Name = name;
            this.Description = description;
        }
    }
}
