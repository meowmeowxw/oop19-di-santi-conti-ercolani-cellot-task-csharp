﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KeepassJ_Conti
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Database MyDb { get; }

        public MainWindow()
        {
            InitializeComponent();
            MyDb = new Database(txtNomeDb.Text);
            MyDb.AddGroup(new Group("Other", "Default Group of Database"));
            RefreshComboBox();
        }

        private void AddEntryBtn(object sender, RoutedEventArgs e)
        {
            if (txtNomeEntry.Text.Length <= 0 || txtUsername.Text.Length <= 0 || txtPassword.Text.Length <= 0 || comboGroup.SelectedIndex == -1)
            {
                lblEntryError.Content = "Riempire i campi";
                return;
            }
            MyDb.ListEntry.Add(new Entry(txtNomeEntry.Text, txtUsername.Text, txtPassword.Text, MyDb.ListGroup[comboGroup.SelectedIndex]));
            txtNomeEntry.Text = "";
            txtUsername.Text = "";
            txtPassword.Text = "";
            WriteXmlBtn(null, null);
        }
        private void AddGroupBtn(object sender, RoutedEventArgs e)
        {
            lblGroupError.Content = "";
            Boolean app = false;
            if (txtNomeGroup.Text.Length > 0)
            {
                if (txtDescription.Text.Length > 0)
                {
                    app = MyDb.AddGroup(new Group(txtNomeGroup.Text, txtDescription.Text));
                }
                else
                {
                    app = MyDb.AddGroup(new Group(txtNomeGroup.Text));
                }
            }

            if (!app)
            {
                lblGroupError.Content = "gruppo non aggiunto";
                return;
            }
            RefreshComboBox();
            txtNomeGroup.Text = "";
            txtDescription.Text = "";
            WriteXmlBtn(null, null);
        }

        private void WriteXmlBtn(object sender, RoutedEventArgs e)
        {
            //lblVideo.Content = "cioooooo  \n ffffffffffff f \n fff";
            lblVideo.Text = ConvertXml.ToXml(MyDb);
            lblGroupError.Content = "";
            lblEntryError.Content = "";
        }

        private void txtNomeDb_TextChanged(object sender, TextChangedEventArgs e)
        {
            MyDb.Nome = txtNomeDb.Text;
            WriteXmlBtn(null, null);
        }

        private void RefreshComboBox()
        {
            List<string> app = new List<string>();
            foreach (Group item in MyDb.ListGroup)
            {
                app.Add(item.Name);
            }
            comboGroup.ItemsSource = app;
        }
    }
}
