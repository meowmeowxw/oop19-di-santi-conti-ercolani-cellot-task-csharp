﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KeepassJ_Conti
{
    class Database
    {
        public string Nome { get; set; }
        public List<Entry> ListEntry = new List<Entry>();
        public List<Group> ListGroup = new List<Group>();

        public Database()
        {
            Nome = "";
        }
        public Database(string nomeDatabase)
        {
            Nome = nomeDatabase;
        }

        public Boolean AddGroup(Group group)
        {
            if (ListGroup.Where(c => c.Name == group.Name).ToList().Count == 0)
            {
                this.ListGroup.Add(group);
                return true;
            }
            return false;
        }
    }
}
