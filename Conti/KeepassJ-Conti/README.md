Nel mio codice in c# ho replicato in parte minima quella che è la creazione delle classi Database, Entry e Group, in cui però non ho replicato tutti i metodi di controllo su inserimento ed eliminazione ma solo uno sull'inserimento di Group duplicati in cui ho usato LINQ per confrontarne l'uso rispetto agli stream su Java.

L'applicativo comprende una Window in cui ho impostato delle label, textbox e button come ho fatto in minima parte nella mia parte nel progetto ed il loro uso come avviene nei Controller che ho creato in Java.

Nella gui sono presenti tre righe principali in cui inserire il nome del database, le entry e i group.
Per poi essere tutto visualizzato o in tempo reale o alla pressione del bottone la conversione in documento XML, creata con l'uso di una classe che cerca di emulare il risultato creato dalla classe ConvertXml del progetto (senza l'uso di librerie esterne ma solo con stringhe apposite).