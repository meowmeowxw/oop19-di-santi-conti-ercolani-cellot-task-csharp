Per scrivere il mio task C# sono partito dalla traduzione delle classi Java create durante
il progetto, in codice c# appunto. Dopodicché sono passato alla realizzazione della grafica
che oltre ad abbellire il tutto mi è stata anche utile per testare il codice da me prodotto.
All'avvio del programma appare una finestra minima con un bottone che se premuto apre la
"MainForm". Al suo interno è possibile testare tutte le funzionalità, come: generazione della
password in modo casuale, rivelare o nascondere il testo della password immessa nella textBox,
visualizzazione della robustezza della password (sia tramite progressBar che tramite testo su label),
controllo della validità della password. Per quest'ultima opzione basta cliccare sul bottone "Enter".

Per la realizzazione del task c# ho adoperato Visual Studio 2019.