﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Keepass
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            txtPassword.UseSystemPasswordChar = true;
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            String password = txtPassword.Text;

            if (Manage.isValid(password))
            {
                MessageBox.Show("Password correct!");
            } else
            {
                MessageBox.Show("Password not valid!");
            }
            

        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            if(txtPassword.UseSystemPasswordChar == false)
            {
                btnShow.Text = "Show";
                txtPassword.UseSystemPasswordChar = true;
            }
            else
            {
                btnShow.Text = "Hide";
                txtPassword.UseSystemPasswordChar = false;
            }

        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            String generatePassword = Manage.generatePassword();
            txtPassword.Text = generatePassword;
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            String password = txtPassword.Text;
            int strength = Strength.getStrength(password);

            /*Control the overhead of the strength*/
            if (strength > 100)
                strength = 100;

            prgStrength.Value = strength;
            lblStrength.Text = strength.ToString() + "%";
        }
    }
}
