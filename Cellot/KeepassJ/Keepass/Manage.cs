﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keepass
{
    class Manage
    {
        /*Methos to check if the password is valid*/
        public static Boolean isValid(String p)
        {
            String password = p;

            if (password.Any(c => char.IsDigit(c)) && password.Length >= 8)
                return true;
            return false;
        }

        /*Method that create a random password*/
        public static String generatePassword()
        {
            String psw;
            while (true)
            {
                psw = randomPassword(8);
                if (Manage.isValid(psw)) {
                    return psw;
                }
            }
        }

        private static String randomPassword(int len)
        {
            Random random = new Random(Environment.TickCount);
            char[] p = new char[len];
            String upperLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            String lowerLetters = "abcdefghijklmnopqrstuvwxyz";
            String numbers = "0123456789";
            String symbols = "!@#$%^&*_=+-/.?";
            String values = upperLetters + lowerLetters + numbers + symbols;

            for (int i = 0; i < len; i++)
            {
                p[i] = values.ElementAt(random.Next(values.Length));
            }

            /*Convert the char vector to string*/
            String a = new String(p);

            return a;
        }
    }
}
