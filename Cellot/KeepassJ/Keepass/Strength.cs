﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keepass
{
    class Strength : CheckCharacters
    {
        private static char[] password;
        public static int getStrength(String p)
        {
            password = p.ToCharArray();

            return getSommatoria() - getDifferenza();
        }

        public static int getLength()
        {
            return password.Length;
        }

        private static int getSommatoria()
        {
            int nCaratteri = getLength() * 6;
            int nUpper = (getLength() - countUpper(password)) * 2;
            int nLower = (getLength() - countLower(password)) * 2;
            int nNumbers = countNumbers(password) * 4;
            int nSpecial = countSpecial(password) * 6;
            return nCaratteri + nUpper + nLower + nNumbers + nSpecial;
        }

        private static int getDifferenza()
        {
            int nConsUpper = getConsecutiveUpper(password) * 2;
            int nConsLower = getConsecutiveLower(password) * 2;
            int nConsNumber = getConsecutiveNumbers(password) * 2;
            int nRepeatChar = getRepeatChar(password) * 2;

            return nConsUpper + nConsLower + nConsNumber + nRepeatChar;
        }
    }
}
