﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Keepass
{
    class CheckCharacters : ConsecutiveCharacters
    {
        /**
         * Return the number of Uppercase letters.
        * @param password
        * @return int
        */
        public static int countUpper(char[] password)
        {
            int count = 0;

            for (int i = 0; i < password.Length; i++)
            {
                if (password[i] >= 'A' && password[i] <= 'Z')
                {
                    count++;
                }
            }
            return count;
        }

        /**
         * Return the number of lowercase letters.
         * @param password
         * @return int
         */
        public static int countLower(char[] password)
        {
            int count = 0;

            for (int i = 0; i < password.Length; i++)
            {
                if (password[i] >= 'a' && password[i] <= 'z')
                {
                    count++;
                }
            }
            return count;
        }

        /**
         * Return the number of digit.
         * @param password
         * @return int
         */
        public static int countNumbers(char[] password)
        {
            int count = 0;

            for (int i = 0; i < password.Length; i++)
            {
                if (password[i] >= '0' && password[i] <= '9')
                {
                    count++;
                }
            }
            return count;
        }

        /**
         * Return the number of special characters.
         * @param password
         * @return int
         */
        public static int countSpecial(char[] password)
        {
            int count = 0;
            String symbols = "!@#$%^&*_=+-/.?";

            for (int i = 0; i < password.Length; i++)
            {
                if (password[i] == '!' || password[i] == '@' || password[i] == '#' || password[i] == '$' || password[i] == '%'
                        || password[i] == '^' || password[i] == '&' || password[i] == '*' || password[i] == '_' || password[i] == '='
                        || password[i] == '+' || password[i] == '-' || password[i] == '/' || password[i] == '.' || password[i] == '?')
                {
                    count++;
                }
            }
            return count;
        }
    }
}
