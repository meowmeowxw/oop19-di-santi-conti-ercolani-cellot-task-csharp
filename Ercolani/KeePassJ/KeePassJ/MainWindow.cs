﻿using System;
using Gtk;
using KeePassJ;

public partial class MainWindow : Gtk.Window
{
    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    protected void CreateNew(object sender, EventArgs e)
    {
        ChooseNameDB window = new ChooseNameDB();
        window.Show();
        Destroy();
    }

    protected void OpenNew(object sender, EventArgs e)
    {
        MessageDialog md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info,
                ButtonsType.Close, "Not implemented");
        md.Run();
        md.Destroy();
    }

    protected void Close(object sender, EventArgs e)
    {
        Application.Quit();
    }

    protected void About(object sender, EventArgs e)
    {
        MessageDialog md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info,
        ButtonsType.Close, "KeePassJ was created by:\n\n"
                            + "· Giovanni Di Santi\n"
                            + "· Francesco Ercolani\n"
                            + "· Massimiliano Conti\n"
                            + "· Davide Cellot");
        md.Run();
        md.Destroy();
    }
}
