﻿using System;
using Gtk;

namespace KeePassJ
{
    public partial class ChooseNameDB : Gtk.Window
    {
        public ChooseNameDB() :
                base(Gtk.WindowType.Toplevel)
        {
            Build();
        }

        protected void Continue(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(entry1.Text) || String.IsNullOrEmpty(entry2.Text)) 
            {
                MessageDialog md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info,
                ButtonsType.Close, "Fields cannot be empty");
                md.Run();
                md.Destroy();
            }
            else
            {
                MessageDialog md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info,
                ButtonsType.Close, "Next database creation is not implemented.\n" +
                "Though, the database name is: " + entry1.Text +
                "\nAnd the database description is: " + entry2.Text);
                md.Run();
                md.Destroy();
                Destroy();

                MainWindow window = new MainWindow();
                window.Show();
            }
        }

        protected void Cancel(object sender, EventArgs e)
        {
            MessageDialog md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info,
            ButtonsType.Close, "You are aborting the creation process, data will be lost.");
            md.Run();
            md.Destroy();
            MainWindow window = new MainWindow();
            window.Show();
            Destroy();

        }

        protected void Close(object o, DeleteEventArgs args)
        {
            MessageDialog md = new MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info,
            ButtonsType.Close, "You are aborting the creation process, data will be lost.");
            md.Run();
            md.Destroy();
            MainWindow window = new MainWindow();
            window.Show();
            Destroy();
        }
    }
}
