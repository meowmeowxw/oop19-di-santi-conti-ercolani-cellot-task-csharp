La mia task c# è consistita nell'implementazione della schermata principale MainWindow.cs (equivalente 
di MainMenuController.java) e della schermata di inserimento di nome e descrizione del database con 
realizzazione delle rispettive interfacce grafiche (minimali rispetto al progetto principale), oltre alla
realizzazione del main per il testing del task, e dei controlli/paletti.
Nelle 10 ore predefinite ho contato anche il tempo speso per la configurazione dell'IDE MonoDevelop utilizzato
per appunto la scrittura del codice in c#, e per la configurazione/comprensione del toolkit GTK.
Ho riscontrato inoltre difficoltà con il GUI designer Stetic integrato nell'IDE MonoDevelop che mi hanno
impedito di realizzare una GUI come quella del progetto KeePassJ.
