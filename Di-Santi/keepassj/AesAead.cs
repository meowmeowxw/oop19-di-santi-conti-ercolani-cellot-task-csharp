using System;
using System.Linq;
using System.Security.Cryptography;

namespace keepassj
{
    public abstract class AesAead:ICryptoCipher
    {
        protected byte[] AssociatedDataLength;
        protected byte[] EncKey;
        protected byte[] MacKey;
        public abstract int IvSize { get; }
        public abstract int KeySize { get; }
        public abstract byte[] Encrypt(byte[] plaintext, byte[] key, byte[] iv, byte[] associatedData);

        public abstract byte[] Decrypt(byte[] plaintext, byte[] key, byte[] iv, byte[] associatedData);
        
        protected void Init(byte[] key, int encSize, int macSize, byte[] associatedData)
        {
            SetKeys(key, encSize, macSize);
            UpdateAssociatedDataLength(associatedData);
        }
        
        protected byte[] ComputeHmac(HMAC hmac, byte[] encrypted, byte[] associatedData, int tagSize)
        {
            var data = associatedData.Concat(encrypted).Concat(this.AssociatedDataLength).ToArray();
            var tag = new byte[tagSize];
            Array.Copy(hmac.ComputeHash(data), 0, tag, 0, tagSize);
            return tag;
        }
        
        private void SetKeys(byte[] key, int encSize, int macSize)
        {
            this.EncKey = new byte[encSize];
            this.MacKey = new byte[macSize];
            Array.Copy(key, 0, this.MacKey, 0, this.MacKey.Length);
            Array.Copy(key, this.MacKey.Length, this.EncKey, 0, this.EncKey.Length);
        }
        
        private void UpdateAssociatedDataLength(byte[] associatedData)
        {
            var length = BitConverter.GetBytes(Convert.ToUInt64(associatedData.Length * 8));
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(length);
            }
            this.AssociatedDataLength = length;
        }
    }
}