namespace keepassj
{
    public interface ICryptoCipher
    {
        int IvSize { get; }
        int KeySize { get; }
        byte[] Encrypt(byte[] plaintext, byte[] key, byte[] iv, byte[] associatedData);
        byte[] Decrypt(byte[] plaintext, byte[] key, byte[] iv, byte[] associatedData);
    }
}